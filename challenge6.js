function getAngkaTerbesarKedua(dataNumbers){
    //Search max value
    let nilaiMaks
    for(let i=0; i<dataNumbers.length; i++){
        if(i == 0){ 
            nilaiMaks = dataNumbers[i] 
        }else{
            if(dataNumbers[i] > nilaiMaks)
                {
                    nilaiMaks = dataNumbers[i];
                }
        }                     
    }
    //Delete max value
    dataNumbers.splice(dataNumbers.indexOf(nilaiMaks), 1)
    
    //Search max value again(second max value)
    let nilaiMaksSecond
    for(let i = 0; i<dataNumbers.length; i++){
        if(i == 0){
            nilaiMaksSecond = dataNumbers[i]
        }else{
            if(dataNumbers[i]>nilaiMaksSecond){
                nilaiMaksSecond = dataNumbers[i]
            }
        }
    }
    return nilaiMaksSecond;
}

//TestCase
const dataAngka = [10, 5, 6, 20, 15, 1, 11, 70, 65, 100]
console.log(getAngkaTerbesarKedua(dataAngka))
//console.log(getAngkaTerbesarKedua(0))
//console.log(getAngkaTerbesarKedua())
