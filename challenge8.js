function getInfoPenjualan(dataPenjualanNovel){
    //variable
    let totalUntung = 0
    let totalModalPros = 0
    let persenUntung
    let maks = 0
    let produk
    let penulis = []
    let maksTerjual = 0
    let maksPenulis

    //logic
    dataPenjualanNovel.forEach(total=>{
	
        //totalKeuntungan
        untung = ((total.hargaJual - total.hargaBeli)*total.totalTerjual)-(total.hargaBeli*total.sisaStok)
        totalUntung += untung
        //totalModal
        sisa = (total.totalTerjual + total.sisaStok)*total.hargaBeli
        totalModalPros += sisa
        //persentase
        persenUntung = Math.round(totalUntung*100/totalModalPros)
        //produkBukuTerlaris
        if(total.totalTerjual>maks){
            maks = total.totalTerjual
            produk = total.namaProduk 
        }

        //penulisTerlaris
        let countPenulis = 0
        let countTerjual = 0
        dataPenjualanNovel.forEach(totalChild=>{
            if(totalChild.penulis==total.penulis){
                countPenulis++
            }
        })

        dataPenjualanNovel.forEach(totalChild=>{
            if(totalChild.penulis==total.penulis){
                countTerjual += totalChild.totalTerjual
            }
        })
        
        if(countTerjual>maksTerjual){
            maksTerjual = countTerjual
            maksPenulis = total.penulis
        }

        penulis.push(countTerjual) 
    });
     //konversi rupiah
     const konversiRP = (numberKonversi) =>{
        let number_string = numberKonversi.toString(),
        strSisa = number_string.length % 3,
        rupiah 	= number_string.substr(0, strSisa),
        ribuan 	= number_string.substr(strSisa).match(/\d{3}/g);
         
     if (ribuan) {
         separator = strSisa ? '.' : '';
         rupiah += separator + ribuan.join('.');
     }
     let rupiahFix = `Rp. ${rupiah}`
     return rupiahFix
     }
     
    //Assign the final value in object objFinal
    let objFinal = {
        totalKeuntungan: konversiRP(totalUntung),
        totalModal: konversiRP(totalModalPros),
        persentaseKeuntungan: `${persenUntung}%`,
        produkBukuTerlaris: produk,
        penulisTerlaris: maksPenulis

    }
    return objFinal
}

//TestCase
const dataPenjualanNovel = [
    {
      idProduct: 'BOOK002421',
      namaProduk: 'Pulang - Pergi',
      penulis: 'Tere Liye',
      hargaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStok: 17
    },
    {
      idProduct: 'BOOK002351',
      namaProduk: 'Selamat Tinggal',
      penulis: 'Tere Liye',
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStok: 20
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Garis Waktu',
      penulis: 'Fiersa Besari',
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStok: 5
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Laskar Pelangi',
      penulis: 'Andrea Hirata',
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStok: 56
    }
  ];

  console.log(getInfoPenjualan(dataPenjualanNovel))
  