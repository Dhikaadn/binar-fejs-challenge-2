function getSplitName(personName){
    //Split the character
    const splitSpace = personName.split(" ")
    let firstName = splitSpace[0]
    let middleName = splitSpace[1]
    let lastName = splitSpace[2]

    //Deklarasi object fullname
    const fullName = {
        firstName: firstName,
        middleName: middleName,
        lastName: lastName
    }

    //Assign the object fullname 
    if(fullName.middleName == undefined && fullName.lastName == undefined){
        fullName.middleName = null
        fullName.lastName = null
        return fullName
    }else if(fullName.middleName == middleName && fullName.lastName == undefined){
        fullName.middleName = null
        fullName.lastName = middleName
        return fullName
    } else if(splitSpace.length>3){
        return "ERROR: This function is only for 3 characters name"
    } else{
        return fullName
    }
}

//TestCase
console.log(getSplitName("Andhika Dian Pratama"))
console.log(getSplitName("Budi Nugroho"))
console.log(getSplitName("Johny"))
console.log(getSplitName("Roro Aji Den Hartono Prawiro"))
//console.log(getSplitName(0))


